# Scorp Challenge Project

# Live demo = https://www.metinarslanturk.com/scorp-challenge

# Execute (without SSR)

1. Clone repo
2. In frontend folder run `yarn install` (of course you can use npm but my advice is always bet on yarn)
3. Then in frontend folder run `yarn start` it will start serve on `http://localhost:3000/` as default.

# Execute (with SSR)

1. Clone repo
2. In frontend folder run `yarn install` (of course you can use npm but my advice is always bet on yarn)
3. Then in frontend folder run `yarn build`
4. Then in server folder run `yarn install`
5. Then in the server folder run `yarn build:dev` it will start serve on `http://localhost:8000/` as default.

# Challenge notes

Firstly thanks for the challenge which you provided. It was a funny weekend work. I want to describe my work in this section with causes. Firstly i want to show a picture of my work timeline for this challenge. I had my internship in Switzerland then after turn back to Turkey, i worked as a freelancer remote developer with same company. In Swiss culture, they believe the keeping working timeline can show talent and approaches of a developer. With this good habit, i kept always my timeline for this kind of project.
![enter image description here](https://www.metinarslanturk.com/ss0.png)

**What did i use?**
This project implemented fully in ReactJS with redux, react-router-dom. I used create-react-app to get rid of boilerplate for webpack and babel (but still i needed to implement much of them, i will explain). I used bootstrap (just css part, jquery part excluded with react-bootstrap library). Literally, i used bootstrap just like TailwindCSS. Just for ready-use css utility classes. The other parts were unnecessary for this project. You can ask so then why you didn't use directly Tailwind? I don't know maybe i am old fashioned boomer for this discuss :) I agree that it is really good library, i like the main logic but in a wide future project i see that it is a strain for project, imho. Anyway, i preferred bootstrap utility classes. I always prefer scss, for tree-shaking modularity of css libraries and effective code management. So, i used scss for styling.
You will see a little animation in titles, that is react-animation-on-scroll library which my own library in NPM. I wanted to use it which something from me :)

Another thing is Typescript... Let me tell a little story with this. Years ago, i had an interview with a company as a junior frontend developer. They asked to me, do you think Typescript is "must to have or nice to have" ? I said, project timeline, deadline is important so it can be nice to have according to project blah blah blah. They didn't like my answer. They were really upset with my answer. After that, i worked as senior front end developer, lead-front end developer many years, i really saw that Typescript is a "must to have". It can really save much time and effort. So it is a habitat for me now and i used it in this challenge and in every my projects.

The position was a lead level position which depends on this challenge. So, after the finishing requirements, i had some extra time and i wanted to show my approach to this kind real world projects. The fashion is implementing server-side-rendering in this kind of web-apps. So i implemented ssr in a nodejs application. Also code-splitting is a huge performance booster. Medium and Trendyol usually prefers it maybe you saw. So i used officially advised code splitting library in React side "Loadable Components". I splitted Contact Us page as an example, you can check it from network tab.

No doubt, these weren't in requirements, just my extra approaches. So whole this ssr and code-split thing is provided what? Of course it has lots of seo improvements beside this i want to show Lighthouse performances:

- Desktop:
  ![enter image description here](https://www.metinarslanturk.com/ss1.png)

Mobile:
![enter image description here](https://www.metinarslanturk.com/ss2.png)

98 in desktop and 97 in mobile performance. Just wow! Wow, because i didn't splitted react bootstrap even. Okay site is not so much big, but generally in bundles the basic libraries take much space not the implemented code. In here, there is react core, helmet, form validation, icons, craco... And 98 as performance, good achievement i think.
Let's deep dive to how i did this. Firstly, i gzipped all provided js and css files. I am serving these static files in NGINX at my server with static_gzip option. Also proxying requests to server side renderer nodejs process. Nodejs (server) part of code again written in Typescript (always bet on Typescript, i mean always :) ). It has babel and webpack configs for building, serving and code splitting configurations.
Let's turn back to frontend side. As i said at the start i used create-react-app to get rid of webpack boilerplate. Unfortunately, the code splitting library needs webpack and babel confgis. In that part, there is an amazing library "craco". Without ejecting create-react-app you can provide webpack and babel configs to your application. It is really useful library and i used it. So in server side directly, in frontend side with craco i provided webpack configs and made achievement of a web-app with functional requirements. It has ssr, caching code splitting and gzip support.

Last but not least, if you examine the server renderer code, it has a built in cache mechanism. When it renders a url, caches it. That cache works with least recently used (LRU) logic. "LRUCache" which used in this project is my own implementation. As soon as possible, i will publish it on NPM. That caching mechanism providing huge performance boost.

Anyway, thanks for reading. If you have questions, don't hesitate to asking to me.
Metin
