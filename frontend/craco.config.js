const LoadablePlugin = require('@loadable/webpack-plugin');

module.exports = {
  webpack: {
    plugins: [new LoadablePlugin()],
  },
  babel: {
    plugins: ['@loadable/babel-plugin'],
  },
};
