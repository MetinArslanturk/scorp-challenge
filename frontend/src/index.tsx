import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import configureStore from './store';
import App from './components/App';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { history } from './history';
import { loadableReady } from '@loadable/component';
import { HelmetProvider } from 'react-helmet-async';
import { setTranslationsAction } from './actions/language';
import { DEFAULT_LANG } from './utils/constants';
import translations from './utils/translations';

const store = configureStore();

store.dispatch(setTranslationsAction(translations[DEFAULT_LANG]));

loadableReady(() => {
  ReactDOM.hydrate(
    <React.StrictMode>
      <Provider store={store}>
        <Router history={history}>
          <HelmetProvider>
            <App />
          </HelmetProvider>
        </Router>
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );
});
