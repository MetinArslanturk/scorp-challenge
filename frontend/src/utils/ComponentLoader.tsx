import React from 'react';
import loadable from '@loadable/component';

export const ContactUsPageLoader = loadable(
  () =>
    import(
      /* webpackChunkName: "contact-us" */ '../components/pages/ContactUsPage'
    ),
  { fallback: <div>Loading..</div> }
);
