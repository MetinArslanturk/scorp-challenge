import { ContactUsPageSchema, HomePageSchema } from './page-schemas';
import { FetchStatusesConstant } from './types';

export const BASE_URL = '/';

export const DEFAULT_LANG = 'en';

export const fetchStatuses: FetchStatusesConstant = {
  init: 'init',
  loading: 'loading',
  success: 'success',
  error: 'error',
};

export const authReducerActionTypes = {
  logout: 'START_LOGOUT',
  login: 'START_LOGIN',
};

export const languageReducerActionTypes = {
  setCurrentLanguage: 'SET_CURRENT_LANGUAGE',
  setTranslations: 'SET_TRANSLATIONS',
};

export const layoutReducerActionTypes = {
  setCurrentPage: 'SET_CURRENT_PAGE',
};

export const pages = [HomePageSchema, ContactUsPageSchema];

export const countries = [
  { id: 'TR', name: 'Turkey' },
  { id: 'US', name: 'United States of America' },
  { id: 'GB', name: 'United Kingdom' },
  { id: 'DE', name: 'Germany' },
  { id: 'SE', name: 'Sweden' },
  { id: 'KE', name: 'Kenya' },
  { id: 'BR', name: 'Brazil' },
  { id: 'ZW', name: 'Zimbabwe' },
];
