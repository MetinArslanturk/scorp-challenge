import { PageSchema } from './types';

export const HomePageSchema: PageSchema = {
  name: 'Homepage',
  endpoint: '',
  translationKey: 'homepage',
};

export const ContactUsPageSchema: PageSchema = {
  name: 'Contact Us',
  endpoint: 'contact-us',
  translationKey: 'contactUs',
};
