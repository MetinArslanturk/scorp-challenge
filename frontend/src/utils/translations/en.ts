export default {
  login: 'Login',
  logout: 'Logout',
  homepage: 'Homepage',
  contactUs: 'Contact Us',
  siteName: 'Sample Project',
  requiredField: 'This field is required',
  cancel: 'Cancel',
  name: 'Name',
  enterName: 'Enter name',
  email: 'E-mail address',
  enterEmail: 'Enter e-mail adress',
  password: 'Password',
  enterPassword: 'Enter password',
  phoneNumber: 'Phone Number',
  phoneNumberPatternWarn: 'Wrong pattern for phone number.',
  emailPatternWarn: 'Wrong pattern for email',
  country: 'Country',
  notFound: 'Not Found',
  selectYourCountry: 'Select your country',
  text: 'Text',
  send: 'Send',
  successSend:
    'Your message sent successfully! Data logged into console, you can check from there.',
  tr: 'Turkey',
  us: 'United States of America',
  gb: 'United Kingdom',
  de: 'Germany',
  se: 'Sweden',
  ke: 'Kenya',
  br: 'Brazil',
  zw: 'Zimbabwe',
  loremText: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec
  vestibulum purus. Quisque aliquam dui quis dolor ultrices vestibulum
  semper eu ante. Vestibulum gravida est at mi placerat mattis. Donec
  tristique, elit in venenatis efficitur, massa risus gravida diam,
  vel semper erat nunc eget eros. Class aptent taciti sociosqu ad
  litora torquent per conubia nostra, per inceptos himenaeos. Donec in
  mauris congue, accumsan augue sit amet, egestas sapien. Vestibulum
  in felis vitae augue lacinia consequat. Phasellus eu porttitor
  metus. Pellentesque sollicitudin ornare lorem nec porta. Morbi
  ultrices, quam ut dapibus mollis, ipsum sem gravida diam, non mollis
  risus diam eget sem. Lorem ipsum dolor sit amet, consectetur
  adipiscing elit. Nunc eget nisi ullamcorper, tristique nisl sit
  amet, accumsan purus. Suspendisse eu rhoncus sem. Morbi et dolor
  lacinia, ultrices ex id, ultrices nunc. Quisque vitae iaculis orci.
  Aliquam eu consectetur orci, in iaculis quam. Vestibulum aliquam
  sapien purus, in vehicula ligula sollicitudin ut. Curabitur id nibh
  ut arcu pellentesque semper sed eu leo. Sed efficitur, quam eu
  venenatis tristique, augue lorem tincidunt ligula, vitae vestibulum
  mi ante ut lacus. Integer augue turpis, rutrum eget dignissim ut,
  consectetur sit amet sapien. Fusce volutpat et justo in facilisis.
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec
  vestibulum purus. Quisque aliquam dui quis dolor ultrices vestibulum
  semper eu ante. Vestibulum gravida est at mi placerat mattis. Donec
  tristique, elit in venenatis efficitur, massa risus gravida diam,
  vel semper erat nunc eget eros. Class aptent taciti sociosqu ad
  litora torquent per conubia nostra, per inceptos himenaeos. Donec in
  mauris congue, accumsan augue sit amet, egestas sapien. Vestibulum
  in felis vitae augue lacinia consequat. Phasellus eu porttitor
  metus. Pellentesque sollicitudin ornare lorem nec porta. Morbi
  ultrices, quam ut dapibus mollis, ipsum sem gravida diam, non mollis
  risus diam eget sem. Lorem ipsum dolor sit amet, consectetur
  adipiscing elit. Nunc eget nisi ullamcorper, tristique nisl sit
  amet, accumsan purus. Suspendisse eu rhoncus sem. Morbi et dolor
  lacinia, ultrices ex id, ultrices nunc. Quisque vitae iaculis orci.`,
};
