export type Translations = {
  [key: string]: string;
};

export type Countries = { id: string; name: string; value: string }[];

export type FetchStatusesConstant = {
  init: 'init';
  loading: 'loading';
  success: 'success';
  error: 'error';
};

export type FetchStatuses = 'init' | 'loading' | 'success' | 'error';

export type PageSchema = {
  name: string;
  endpoint: string;
  translationKey: string;
};
export type User = {
  name: string;
  email: string;
};

export type FetchUser = {
  data: User | null;
  status: FetchStatuses;
  payload: any;
};

export type AuthStore = {
  fetchUserInfo: FetchUser;
};

export type LanguageStore = {
  currentLanguage: string;
  translations: Translations;
};

export type LayoutStore = {
  currentPage: PageSchema;
};

export type StoreState = {
  auth: AuthStore;
  language: LanguageStore;
  layout: LayoutStore;
};

export type DispatchFunc = (action: any) => any;

export type unknownFunc = any;
