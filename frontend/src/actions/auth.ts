import { authReducerActionTypes, fetchStatuses } from '../utils/constants';
import { DispatchFunc, User } from '../utils/types';

export const loginRequestAction = (
  name: string,
  email: string,
  password: string
) => {
  return (dispatch: DispatchFunc) => {
    try {
      dispatch({
        type: authReducerActionTypes.login,
        userInfo: null,
        status: fetchStatuses.loading,
        payload: undefined,
      });
      setTimeout(() => {
        dispatch({
          type: authReducerActionTypes.login,
          userInfo: { name, email } as User,
          status: fetchStatuses.success,
          payload: undefined,
        });
        return true;
      }, 1000);
    } catch (_err) {
      dispatch({
        type: authReducerActionTypes.login,
        userInfo: null,
        status: fetchStatuses.error,
        payload: _err.message,
      });
      return false;
    }
  };
};

export const logoutRequestAction = () => ({
  type: authReducerActionTypes.logout,
});
