import { layoutReducerActionTypes } from '../utils/constants';
import { PageSchema } from '../utils/types';

export const setCurrentPageAction = (currentPage: PageSchema) => ({
  type: layoutReducerActionTypes.setCurrentPage,
  currentPage,
});
