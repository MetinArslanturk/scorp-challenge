import { languageReducerActionTypes } from '../utils/constants';
import { DispatchFunc, Translations } from '../utils/types';
import translations from '../utils/translations';

export const setTranslationsAction = (translations: Translations) => ({
  type: languageReducerActionTypes.setTranslations,
  translations,
});

export const setCurrentLanguageAction = (currentLanguage: string) => {
  return (dispatch: DispatchFunc) => {
    dispatch(setTranslationsAction(translations[currentLanguage]));
    dispatch({
      type: languageReducerActionTypes.setCurrentLanguage,
      currentLanguage,
    });
  };
};
