import { createBrowserHistory } from 'history';

export const history: any =
  typeof window !== 'undefined' ? createBrowserHistory() : undefined;
