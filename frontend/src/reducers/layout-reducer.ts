import { layoutReducerActionTypes } from '../utils/constants';
import { LayoutStore, PageSchema } from '../utils/types';

const languageDefaultState: LayoutStore = {
  currentPage: {} as PageSchema,
};

export default (state = languageDefaultState, action: any) => {
  switch (action.type) {
    case layoutReducerActionTypes.setCurrentPage:
      return {
        ...state,
        currentPage: action.currentPage,
      };
    default:
      return state;
  }
};
