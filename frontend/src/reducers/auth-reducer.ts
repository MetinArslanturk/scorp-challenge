import { authReducerActionTypes, fetchStatuses } from '../utils/constants';
import { AuthStore } from '../utils/types';

const authDefaultState: AuthStore = {
  fetchUserInfo: { data: null, status: fetchStatuses.init, payload: undefined },
};

export default (state = authDefaultState, action: any) => {
  switch (action.type) {
    case authReducerActionTypes.login:
      return {
        ...state,
        fetchUserInfo: {
          data: action.userInfo,
          status: action.status,
          payload: action.payload,
        },
      };

    case authReducerActionTypes.logout:
      return {
        ...state,
        fetchUserInfo: {
          data: null,
          status: fetchStatuses.init,
          payload: undefined,
        },
      };

    default:
      return state;
  }
};
