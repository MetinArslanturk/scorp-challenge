import { DEFAULT_LANG, languageReducerActionTypes } from '../utils/constants';
import { LanguageStore } from '../utils/types';

const languageDefaultState: LanguageStore = {
  currentLanguage: DEFAULT_LANG,
  translations: {},
};

export default (state = languageDefaultState, action: any) => {
  switch (action.type) {
    case languageReducerActionTypes.setCurrentLanguage:
      return {
        ...state,
        currentLanguage: action.currentLanguage,
      };

    case languageReducerActionTypes.setTranslations:
      return {
        ...state,
        translations: action.translations,
      };

    default:
      return state;
  }
};
