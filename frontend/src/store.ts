import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import authReducer from './reducers/auth-reducer';
import languageReducer from './reducers/language-reducer';
import layoutReducer from './reducers/layout-reducer';

export default () => {
  const store = createStore(
    combineReducers({
      auth: authReducer,
      language: languageReducer,
      layout: layoutReducer,
    }),
    applyMiddleware(thunk)
  );

  return store;
};
