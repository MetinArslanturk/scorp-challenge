import React, { useEffect } from 'react';
import { AnimationOnScroll } from 'react-animation-on-scroll';
import { Helmet } from 'react-helmet-async';
import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { setCurrentPageAction } from '../../actions/layout';
import { HomePageSchema } from '../../utils/page-schemas';
import {
  DispatchFunc,
  PageSchema,
  StoreState,
  Translations,
} from '../../utils/types';

type Props = {
  translations: Translations;
  setCurrentPage: DispatchFunc;
};

const HomePage = ({ translations, setCurrentPage }: Props) => {
  useEffect(() => {
    setCurrentPage(HomePageSchema);
  }, [setCurrentPage]);
  return (
    <>
      <Helmet>
        <title>{translations['homepage']} - Sample Project</title>
      </Helmet>
      <Card>
        <Card.Body>
          <AnimationOnScroll animateIn="animate__bounceIn">
            <Card.Title>{translations['homepage']}</Card.Title>
          </AnimationOnScroll>

          <Card.Text>
            {translations['loremText']} <br />
            <br /> {translations['loremText']}
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  translations: state.language.translations,
});

const mapDispatchToProps = () => (dispatch: DispatchFunc) => ({
  setCurrentPage: (currentPage: PageSchema) =>
    dispatch(setCurrentPageAction(currentPage)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
