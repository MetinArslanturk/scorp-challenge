import React, { useEffect, useState } from 'react';
import { Alert, Button, Card, Col, Form, Row, Spinner } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import SelectSearch from 'react-select-search';
import { useForm } from 'react-hook-form';
import { countries, fetchStatuses } from '../../utils/constants';
import {
  Countries,
  DispatchFunc,
  FetchStatuses,
  PageSchema,
  StoreState,
  Translations,
} from '../../utils/types';
import {
  emailPatternRegex,
  phoneNumberPatternRegex,
} from '../../utils/helpers';
import { connect } from 'react-redux';
import { ContactUsPageSchema } from '../../utils/page-schemas';
import { setCurrentPageAction } from '../../actions/layout';
import { AnimationOnScroll } from 'react-animation-on-scroll';

type Props = {
  translations: Translations;
  setCurrentPage: DispatchFunc;
};

const ContactUsPage = ({ translations, setCurrentPage }: Props) => {
  useEffect(() => {
    setCurrentPage(ContactUsPageSchema);
  }, [setCurrentPage]);

  const [sendStatus, setSendStatus] = useState<FetchStatuses>(
    fetchStatuses.init
  );

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    getValues,
    formState: { errors },
  } = useForm({
    mode: 'all',
  });

  const onSubmit = (data: any) => {
    setSendStatus(fetchStatuses.loading);

    setTimeout(() => {
      console.log(data);
      setSendStatus(fetchStatuses.success);
    }, 1000);
  };

  return (
    <>
      <Helmet>
        <title>{translations['contactUs']} - Sample Project</title>
      </Helmet>
      <Card>
        <Card.Body>
          <AnimationOnScroll animateIn="animate__bounceIn">
            <Card.Title>{translations['contactUs']}</Card.Title>
          </AnimationOnScroll>

          <Row className="w-100 d-flex justify-content-center contact-us-form">
            <Col xs={12} md={6}>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="name">
                  <Form.Label>{translations['name']}</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={translations['enterName']}
                    {...register('name', { required: true })}
                  />
                  <Form.Text className="text-muted">
                    {errors.name && (
                      <span>* {translations['requiredField']}</span>
                    )}
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId="email">
                  <Form.Label>{translations['email']}</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder={translations['enterEmail']}
                    {...register('email', {
                      required: true,
                      pattern: emailPatternRegex,
                    })}
                  />
                  <Form.Text className="text-muted">
                    {errors.email &&
                      (errors.email.type === 'required' ? (
                        <span>* {translations['requiredField']}</span>
                      ) : (
                        errors.email.type === 'pattern' && (
                          <span>* {translations['emailPatternWarn']}</span>
                        )
                      ))}
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId="phonenumber">
                  <Form.Label>{translations['phoneNumber']}</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="05xxxxxxxxx"
                    {...register('phonenumber', {
                      required: true,
                      pattern: phoneNumberPatternRegex,
                    })}
                  />
                  <Form.Text className="text-muted">
                    {errors.phonenumber &&
                      (errors.phonenumber.type === 'required' ? (
                        <span>* {translations['requiredField']}</span>
                      ) : (
                        errors.phonenumber.type === 'pattern' && (
                          <span>
                            * {translations['phoneNumberPatternWarn']}
                          </span>
                        )
                      ))}
                  </Form.Text>
                </Form.Group>
                <div>
                  <Form.Label>{translations['country']}</Form.Label>
                  <SelectSearch
                    // @ts-ignore
                    onBlur={() => {
                      const data = getValues();
                      if (!data.country_code) {
                        setError('country_code', {
                          type: 'required',
                          message: '',
                        });
                      }
                    }}
                    onChange={(value) => {
                      setValue('country_code', value, {
                        shouldValidate: true,
                      });
                    }}
                    options={(countries as Countries).map((c) => {
                      c.value = c.id;
                      c.name = translations[c.id.toLowerCase()];
                      return c;
                    })}
                    renderValue={(valueProps) => {
                      const val = getValues();
                      const inputVal = val?.country_code
                        ? translations[val.country_code.toLowerCase()]
                        : valueProps.value;
                      return (
                        // @ts-ignore
                        <input
                          {...valueProps}
                          value={inputVal}
                          className="select-search__input"
                        />
                      );
                    }}
                    search
                    filterOptions={(countries) => {
                      return (value) => {
                        return countries.filter((c) =>
                          (c.name.toLowerCase() as string).includes(
                            value.toLowerCase()
                          )
                        );
                      };
                    }}
                    emptyMessage={translations['notFound']}
                    placeholder={translations['selectYourCountry']}
                  />
                  <Form.Text className="text-muted">
                    {errors.country_code && (
                      <span>* {translations['requiredField']}</span>
                    )}
                  </Form.Text>
                </div>
                <Form.Group controlId="text" className="mt-2">
                  <Form.Label>{translations['text']}</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    {...register('text', { required: true })}
                  />
                  <Form.Text className="text-muted">
                    {errors.text && (
                      <span>* {translations['requiredField']}</span>
                    )}
                  </Form.Text>
                </Form.Group>
              </Form>
              <div className="d-flex justify-content-end mt-3">
                <Button
                  disabled={sendStatus === fetchStatuses.loading}
                  onClick={() => {
                    const data = getValues();
                    if (!data.country_code) {
                      setError('country_code', {
                        type: 'required',
                        message: '',
                      });
                    }
                    handleSubmit(onSubmit)();
                  }}
                  variant="primary"
                  type="submit"
                >
                  {sendStatus === fetchStatuses.loading ? (
                    <Spinner animation="border" size="sm" />
                  ) : (
                    translations['send']
                  )}
                </Button>
              </div>
            </Col>
          </Row>
          {sendStatus === fetchStatuses.success && (
            <Alert className="mt-2" key={'success-send'} variant="success">
              {translations['successSend']}
            </Alert>
          )}
        </Card.Body>
      </Card>
    </>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  translations: state.language.translations,
});

const mapDispatchToProps = () => (dispatch: DispatchFunc) => ({
  setCurrentPage: (currentPage: PageSchema) =>
    dispatch(setCurrentPageAction(currentPage)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsPage);
