import React, { useEffect } from 'react';
import { Button, Form, Modal, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { loginRequestAction } from '../../actions/auth';
import { fetchStatuses } from '../../utils/constants';
import { emailPatternRegex } from '../../utils/helpers';
import {
  DispatchFunc,
  FetchUser,
  StoreState,
  unknownFunc,
  Translations,
} from '../../utils/types';
import LanugageToggle from './LanguageToggle';

type Props = {
  showModal: boolean;
  setShowModal: (value: boolean) => void;
  fetchUserInfo: FetchUser;
  translations: Translations;
  loginRequest: unknownFunc;
};
const LoginModal = ({
  showModal,
  setShowModal,
  translations,
  loginRequest,
  fetchUserInfo,
}: Props) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: 'all',
  });

  useEffect(() => {
    if (fetchUserInfo.status === fetchStatuses.success) {
      reset();
      setShowModal(false);
    }
  }, [fetchUserInfo, setShowModal, reset]);

  const onSubmit = ({
    name,
    email,
    password,
  }: {
    name: string;
    email: string;
    password: string;
  }) => {
    loginRequest(name, email, password);
  };

  return (
    <Modal
      show={showModal}
      onHide={() => setShowModal(false)}
      animation={false}
    >
      <Modal.Header closeButton>
        <Modal.Title>Login</Modal.Title>
        <div className="d-flex justify-content-end w-100">
          <LanugageToggle />
        </div>
      </Modal.Header>

      <Modal.Body className="login-modal">
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group controlId="name">
            <Form.Label>{translations['name']}</Form.Label>
            <Form.Control
              type="text"
              placeholder={translations['enterName']}
              {...register('name', { required: true })}
            />
            <Form.Text className="text-muted">
              {errors.name && <span>* {translations['requiredField']}</span>}
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="email">
            <Form.Label>{translations['email']}</Form.Label>
            <Form.Control
              type="email"
              placeholder={translations['enterEmail']}
              {...register('email', {
                required: true,
                pattern: emailPatternRegex,
              })}
            />
            <Form.Text className="text-muted">
              {errors.email &&
                (errors.email.type === 'required' ? (
                  <span>* {translations['requiredField']}</span>
                ) : (
                  errors.email.type === 'pattern' && (
                    <span>* {translations['emailPatternWarn']}</span>
                  )
                ))}
            </Form.Text>
          </Form.Group>
          <Form.Group controlId="userPassword">
            <Form.Label>{translations['password']}</Form.Label>
            <Form.Control
              type="password"
              placeholder={translations['enterPassword']}
              {...register('userPassword', { required: true })}
            />
            <Form.Text className="text-muted">
              {errors.userPassword && (
                <span>* {translations['requiredField']}</span>
              )}
            </Form.Text>
          </Form.Group>
        </Form>
      </Modal.Body>

      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            reset();
            setShowModal(false);
          }}
        >
          {translations['cancel']}
        </Button>
        <Button
          variant="primary"
          disabled={fetchUserInfo.status === fetchStatuses.loading}
          onClick={handleSubmit(onSubmit)}
        >
          {fetchUserInfo.status === fetchStatuses.loading ? (
            <Spinner animation="border" size="sm" />
          ) : (
            translations['login']
          )}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  translations: state.language.translations,
  fetchUserInfo: state.auth.fetchUserInfo,
});

const mapDispatchToProps = () => (dispatch: DispatchFunc) => ({
  loginRequest: (name: string, email: string, password: string) =>
    dispatch(loginRequestAction(name, email, password)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
