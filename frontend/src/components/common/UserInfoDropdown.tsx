import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { connect } from 'react-redux';
import { logoutRequestAction } from '../../actions/auth';
import {
  DispatchFunc,
  FetchUser,
  StoreState,
  Translations,
  unknownFunc,
} from '../../utils/types';

type Props = {
  translations: Translations;
  fetchUserInfo: FetchUser;
  logoutRequest: unknownFunc;
};

const UserInfoDropdown = ({
  translations,
  fetchUserInfo,
  logoutRequest,
}: Props) => {
  return (
    <Dropdown>
      <Dropdown.Toggle size="sm" id="dropdown-basic" variant="outline-dark">
        {fetchUserInfo.data?.name}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item disabled>{fetchUserInfo.data?.email}</Dropdown.Item>
        <Dropdown.Item onClick={() => logoutRequest()}>
          {translations['logout']}
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  translations: state.language.translations,
  fetchUserInfo: state.auth.fetchUserInfo,
});

const mapDispatchToProps = () => (dispatch: DispatchFunc) => ({
  logoutRequest: () => dispatch(logoutRequestAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserInfoDropdown);
