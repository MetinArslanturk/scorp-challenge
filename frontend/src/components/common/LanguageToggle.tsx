import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { connect } from 'react-redux';
import { setCurrentLanguageAction } from '../../actions/language';
import languages from '../../utils/translations';
import { DispatchFunc, StoreState } from '../../utils/types';

type Props = {
  currentLanguage: string;
  setCurrentLanguage: DispatchFunc;
};

const LanugageToggle = ({ currentLanguage, setCurrentLanguage }: Props) => {
  return (
    <Dropdown className="language-button">
      <Dropdown.Toggle
        size="sm"
        id="dropdown-basic"
        variant="outline-secondary"
      >
        {currentLanguage?.toUpperCase()}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {Object.keys(languages).map((lang) => {
          return (
            <Dropdown.Item
              key={lang}
              active={currentLanguage === lang}
              onClick={() => setCurrentLanguage(lang)}
            >
              {lang.toUpperCase()}
            </Dropdown.Item>
          );
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  currentLanguage: state.language.currentLanguage,
});

const mapDispatchToProps = () => (dispatch: DispatchFunc) => ({
  setCurrentLanguage: (currentLanguage: string) =>
    dispatch(setCurrentLanguageAction(currentLanguage)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LanugageToggle);
