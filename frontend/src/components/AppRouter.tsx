import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import HomePage from '../components/pages/HomePage';
import { ContactUsPageLoader } from '../utils/ComponentLoader';
import { BASE_URL } from '../utils/constants';

const AppRouter = () => {
  return (
    <>
      <Switch>
        <Route path={BASE_URL + ''} component={HomePage} exact />
        <Route
          path={BASE_URL + 'contact-us'}
          component={ContactUsPageLoader}
          exact
        />
        <Redirect to={BASE_URL} />
      </Switch>
    </>
  );
};

export default AppRouter;
