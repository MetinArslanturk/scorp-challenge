import React from 'react';
import { Container } from 'react-bootstrap';
import Footer from './layout/Footer';
import Header from './layout/Header';
import MainArea from './layout/MainArea';

const App = () => {
  return (
    <>
      <Header />
      <Container fluid="xl">
        <div className="main-container">
          <MainArea />
        </div>
      </Container>
      <Footer />
    </>
  );
};

export default App;
