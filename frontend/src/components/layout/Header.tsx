import { Link } from 'react-router-dom';
// @ts-ignore
import { slide as Menu } from 'react-burger-menu';
import { GoThreeBars, GoX } from 'react-icons/go';
import React, { useMemo, useState } from 'react';
import { BASE_URL, fetchStatuses, pages } from '../../utils/constants';
import LanugageToggle from '../common/LanguageToggle';
import { Button } from 'react-bootstrap';
import UserInfoDropdown from '../common/UserInfoDropdown';
import LoginModal from '../common/LoginModal';
import {
  FetchUser,
  PageSchema,
  StoreState,
  Translations,
} from '../../utils/types';
import { connect } from 'react-redux';
import { HomePageSchema } from '../../utils/page-schemas';

type Props = {
  translations: Translations;
  fetchUserInfo: FetchUser;
  currentPage: PageSchema;
};

const Header = ({ translations, fetchUserInfo, currentPage }: Props) => {
  const [showBurgerMenu, setShowBurgerMenu] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);
  const userLoggedIn = useMemo(
    () => fetchUserInfo.data && fetchUserInfo.status === fetchStatuses.success,
    [fetchUserInfo]
  );
  return (
    <>
      <Menu
        right
        customBurgerIcon={false}
        disableAutoFocus
        isOpen={showBurgerMenu}
        onClose={() => setShowBurgerMenu(false)}
      >
        <div className="menu-wrapper">
          <div className="close-menu-button">
            <div
              className="cursor-pointer"
              onClick={() => setShowBurgerMenu(false)}
            >
              <GoX size={25} />
            </div>
          </div>
          <div>
            <ul className="mobile-menu-items">
              {pages.map((page) => (
                <li key={page.name}>
                  <Link to={BASE_URL + page.endpoint}>
                    {translations[page.translationKey]}
                  </Link>
                </li>
              ))}
            </ul>

            <div className="mobile-user-info">
              {userLoggedIn ? (
                <UserInfoDropdown />
              ) : (
                <Button
                  onClick={() => {
                    setShowBurgerMenu(false);
                    setShowLoginModal(true);
                  }}
                  variant="outline-dark"
                  size="sm"
                >
                  {translations['login']}
                </Button>
              )}
            </div>

            <div className="mobile-language-toggle">
              <LanugageToggle />
            </div>
          </div>
        </div>
      </Menu>
      <div className="bar-wrapper">
        <div className="container-xl">
          <div className="header-items">
            <div className="d-flex align-items-baseline">
              <Link to={BASE_URL}>
                <div className="brand">
                  <picture>
                    <source media="(min-width:768px)" srcSet="/logo.png" />
                    <img
                      className="logo"
                      src="/logo-mobile.png"
                      alt="MetinArslanturk"
                    />
                  </picture>
                </div>
              </Link>
              <div className="page-name ml-1">
                {
                  translations[
                    currentPage.name !== HomePageSchema.name
                      ? currentPage.translationKey
                      : 'siteName'
                  ]
                }
              </div>
            </div>
            <div className="d-none d-md-flex navbar-items">
              {pages.map((page) => (
                <Link
                  key={page.name}
                  to={BASE_URL + page.endpoint}
                  className="navigation-link"
                >
                  {translations[page.translationKey]}
                </Link>
              ))}
              <div className="ml-2">
                {userLoggedIn ? (
                  <UserInfoDropdown />
                ) : (
                  <Button
                    onClick={() => setShowLoginModal(true)}
                    variant="outline-dark"
                    size="sm"
                  >
                    {translations['login']}
                  </Button>
                )}
              </div>
              <div className="ml-4">
                <LanugageToggle />
              </div>
            </div>

            <div className="d-md-none">
              <div
                className="cursor-pointer"
                onClick={() => setShowBurgerMenu(true)}
              >
                <GoThreeBars size={25} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <LoginModal showModal={showLoginModal} setShowModal={setShowLoginModal} />
    </>
  );
};

const mapStateToProps = () => (state: StoreState) => ({
  translations: state.language.translations,
  fetchUserInfo: state.auth.fetchUserInfo,
  currentPage: state.layout.currentPage,
});

export default connect(mapStateToProps)(Header);
