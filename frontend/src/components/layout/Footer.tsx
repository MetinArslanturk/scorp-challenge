import React from 'react';
import { BsHeartFill } from 'react-icons/bs';

const Footer = () => {
  return (
    <>
      <div className="bar-wrapper">
        <div className="container-xl">
          <div className="footer-items">
            <div>
              Made with <BsHeartFill /> All rights reserved in Metin
              Arslantürk's secret safe.
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
