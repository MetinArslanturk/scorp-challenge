import React from 'react';
import AppRouter from '../AppRouter';

const MainArea = () => {
  return (
    <div className="main-content">
      <AppRouter />
    </div>
  );
};

export default MainArea;
