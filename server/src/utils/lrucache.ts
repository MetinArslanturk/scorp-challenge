export default class LRUCache {
  maxSize: number;
  maxTime: number;
  currentSize: number;
  cache: {
    [key: string]: any;
  };
  constructor(maxSize: number, maxTime: number = 432000000) {
    this.maxSize = maxSize;
    this.maxTime = maxTime;
    this.currentSize = 0;
    this.cache = {};
  }

  set(key: string, value: any) {
    if (this.cache[key]) {
      const existingData = this.cache[key];
      existingData.val = value;
      existingData.lastDate = Date.now();
      return;
    }
    if (this.currentSize === this.maxSize) {
      const minKey = this.findOldest();
      delete this.cache[minKey];
      this.currentSize--;
    }
    this.cache[key] = { val: value, lastDate: Date.now() };
    this.currentSize++;
  }

  get(key: string) {
    const data = this.cache[key];
    if (data) {
      if (Date.now() - data.lastDate >= this.maxTime) {
        return null;
      }
      data.lastDate = Date.now();
      return data.val;
    }
    return null;
  }

  async invalidate(keys: string[]) {
    keys.forEach((key) => {
      delete this.cache[key];
      this.currentSize--;
    });
  }

  async invalidateStartWith(keys: string[]) {
    keys.forEach((ky) => {
      for (let k in Object.keys(this.cache)) {
        if (k.startsWith(ky)) {
          delete this.cache[k];
          this.currentSize--;
          break;
        }
      }
    });
  }

  reset() {
    this.cache = {};
    this.currentSize = 0;
  }

  size() {
    return this.currentSize;
  }

  findOldest() {
    let minKey = '';
    let minValue = Infinity;
    for (const [key, value] of Object.entries(this.cache)) {
      if (value.lastDate < minValue) {
        minKey = key;
        minValue = value.lastDate;
      }
    }
    return minKey;
  }
}
