import fs from 'fs';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { HelmetProvider, FilledContext } from 'react-helmet-async';
import { ChunkExtractor } from '@loadable/server';
import indexHtml from '../utils/indexHtmlSchema';
import configureStore from '../../../frontend/src/store';
import App from '../../../frontend/src/components/App';
import LRUCache from '../utils/lrucache';
import { setTranslationsAction } from '../../../frontend/src/actions/language';
import translations from '../../../frontend/src/utils/translations';
import { DEFAULT_LANG } from '../../../frontend/src/utils/constants';

export class RenderService {
  cache: LRUCache;
  extractor: ChunkExtractor;
  indexWithoutSSR: string;

  constructor(cache: LRUCache) {
    this.cache = cache;
  }

  renderHtml(req: any) {
    try {
      const cacheData = this.cache.get(req.url);
      if (cacheData) {
        return cacheData;
      }
      const store = configureStore();
      store.dispatch(setTranslationsAction(translations[DEFAULT_LANG]));
      const helmetContext = {};

      // @ts-ignore
      this.extractor.chunks = [];
      const jsx = this.extractor.collectChunks(
        <Provider store={store}>
          <StaticRouter location={req.url} context={{}}>
            <HelmetProvider context={helmetContext}>
              <App />
            </HelmetProvider>
          </StaticRouter>
        </Provider>
      );

      const html = ReactDOMServer.renderToString(jsx);

      const { helmet } = helmetContext as FilledContext;

      const renderedHtml = indexHtml
        .replace(
          '</head>',
          `  ${helmet.title.toString()}
      ${helmet.meta.toString()}
      ${this.extractor.getStyleTags()}</head>`
        )
        .replace('</body>', `${this.extractor.getScriptTags()}</body>`)
        .replace('<div id="root"></div>', `<div id="root">${html}</div>`);

      this.cache.set(req.url, renderedHtml);
      return renderedHtml;
    } catch (err) {
      console.error(`Error while rendering ${req.url}`);
      return this.indexWithoutSSR;
    }
  }

  readIndexWithoutSSR(frontendBuildPath: string) {
    this.indexWithoutSSR = fs
      .readFileSync(path.resolve(frontendBuildPath, 'index.html'))
      .toString();
  }

  setExtractor(frontendBuildPath: string) {
    const statsFile = path.resolve(frontendBuildPath, 'loadable-stats.json');
    try {
      const stats = JSON.parse(fs.readFileSync(statsFile).toString());
      this.extractor = new ChunkExtractor({ stats });
    } catch (err) {
      console.error(
        `ERROR: No build file found in ${statsFile} or read file failed. You may forget the build frontend or the path is wrong`
      );
      process.exit();
    }
  }
}
