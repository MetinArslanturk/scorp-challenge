import express from 'express';
import renderRoutesBuilder from './routes/renderRoutes';
import { RenderService } from './services/renderService';
import LRUCache from './utils/lrucache';

const app = express();
const PORT = 8000;

const renderCache = new LRUCache(300);
const renderService = new RenderService(renderCache);
const renderRoutes = renderRoutesBuilder(renderService);

app.get('/health', (req, res) => res.send('Express + TypeScript Server is up'));

app.use(renderRoutes);

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT} `);
});
