import express from 'express';
import path from 'path';
import fs from 'fs';
import { RenderService } from '../services/renderService';

const router = express.Router();

let renderService: RenderService;

/* Path / build file checks */
const frontendBuildPath = process.argv[2];
if (!frontendBuildPath) {
  console.error('ERROR: You must give front end files build path as argument!');
  process.exit();
}

/* Router declarations */

router.get('/', (req, res) => {
  return serverRenderer(req, res);
});

router.use(
  express.static(path.resolve(frontendBuildPath), {
    maxAge: '30d',
    index: false,
  })
);

router.get('*', (req, res) => {
  return serverRenderer(req, res);
});

function serverRenderer(req, res) {
  return res.send(renderService.renderHtml(req));
}

export default (rService) => {
  renderService = rService;
  renderService.setExtractor(frontendBuildPath);
  renderService.readIndexWithoutSSR(frontendBuildPath);
  return router;
};
