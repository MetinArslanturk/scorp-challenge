const path = require('path');
const { loadableTransformer } = require('loadable-ts-transformer');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');

module.exports = {
  entry: './src/index.ts',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
        options: {
          getCustomTransformers: () => ({ before: [loadableTransformer] }),
        },
      },
      {
        test: /\.(gif|jpe?g|png|ico)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'static/media/[name].[hash:8].[ext]',
              emitFile: false,
            },
          },
        ],
      },
      { test: /\.s?css$/, loader: 'ignore-loader' },
    ],
  },
  plugins: [
    new WebpackShellPluginNext({
      onBuildEnd: {
        scripts:
          process.env.NODE_ENV === 'development' ? ['yarn run start:dev'] : [],
        blocking: false,
        parallel: true,
      },
    }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  ignoreWarnings: [(w) => w !== 'CriticalDependenciesWarning'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
